﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StatisticsManagerCSharp
{
    class Program
    {
        static void Main(string[] args)
        {
            double[] scores = { 88, 75, 92, 96, 62, 100, 59, 77, 22 };

            double? avg = GetAverage(scores);
            double? mean = GetMedian(scores);
            double? largestVal = GetLargestValue(scores);
            double? smallestVal = GetSmallestValue(scores);

            Console.WriteLine($"Average: {avg}");
            Console.WriteLine($"Mean: {mean}");
            Console.WriteLine($"Largest Val: {largestVal}");
            Console.WriteLine($"Smallest Val: {smallestVal}");
        }

        public static double GetAverage(double[] scores)
        {

            double sum = 0;
            double avg;

            // Check if the array has values        
            if (scores == null || scores.Length == 0)
            {
                throw new ArgumentException("Array is empty.");
            }

            for (int i = 0; i < scores.Length; i++)
            {
                sum += scores[i];
            }

            avg = sum / scores.Length;

            return avg;
        }
        public static double GetMedian(double[] scores)
        {
            // Check if the array has values        
            if (scores == null || scores.Length == 0)
            {
                throw new ArgumentException("Array is empty.");
            }

            // Sort the array
            Array.Sort(scores);

            // Calculate the mscoresedian
            int size = scores.Length;
            int midPoint = size / 2;
            double medianValue =0;

            if (size % 2 == 0)
            {
                double firstValue = scores[(scores.Length / 2) - 1];
                double secondValue = scores[(scores.Length / 2)];
                medianValue = (firstValue + secondValue) / 2.0;
            }
            if (size % 2 == 1)
            {
                medianValue = scores[(scores.Length / 2)];
            }
            return medianValue;
        }
        public static double GetSmallestValue(double[] scores)
        {
            double min = scores.Min();
            return min;
        }
        public static double GetLargestValue(double[] scores)
        {
            double max = scores.Max();
            return max;
        }
    }
}
